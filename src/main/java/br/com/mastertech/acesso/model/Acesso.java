package br.com.mastertech.acesso.model;

import javax.persistence.*;

@Entity
@Table(name = "acessos")
public class Acesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long clienteId;
    private Long portalId;

    public Acesso() {
    }

    public Acesso(Long clienteId, Long portalId) {
        this.clienteId = clienteId;
        this.portalId = portalId;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public Acesso setClienteId(Long clienteId) {
        this.clienteId = clienteId;
        return this;
    }

    public Long getPortalId() {
        return portalId;
    }

    public Acesso setPortalId(Long portalId) {
        this.portalId = portalId;
        return this;
    }
}
